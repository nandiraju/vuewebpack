import Vue from 'vue'
import Home from './screens/Home.vue'
import About from './screens/About.vue'
import Dashboard from './screens/Dashboard.vue'
import Maslayout from './screens/Maslayout.vue'
import Tablelayout from './screens/Tablelayout.vue'
import Login from './screens/Login.vue'

import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    { path: '/', component: Home },
    { path: '/about', component: About },
    { path: '/dashboard', component: Dashboard },
    { path: '/mas', component: Maslayout },
    { path: '/table', component: Tablelayout },
    { path: '/login', component: Login },
];

export default new VueRouter({
    routes,
});