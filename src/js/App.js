 App = {};
 App = {

     logout: function() {
         document.location.href = "#/login";
     },

     showFullScreen: function() {
         $(".side-wrapper").hide();
         $(".nav").hide();
     },

     hideFullScreen: function() {
         $(".side-wrapper").show();
         $(".nav").show();
     },

     toggleSideNav: function(e) {
         $(".side-wrapper").toggleClass("hide-side-bar");
         $(".main-wrapper").toggleClass("expand-main-wrapper");
         $(".stage").toggleClass("stage-padded");
     },

     init: function() {
         NProgress.configure({
             showSpinner: false
         });

         $(document).ajaxStart(function() {
             NProgress.start();
         });

         $(document).ajaxStop(function() {
             NProgress.done();
         });

         App.toggleSideNav();
         $("#side-nav").on("click", function(e) {
             e.preventDefault();
             App.toggleSideNav();
         });

         $('#full-screen').click(function() {
             $(document).toggleFullScreen();
             return false;
         });

         $(".ui.dropdown").dropdown({
             allowCategorySelection: true,
             transition: "fade up",
             context: 'sidebar',
             on: "hover"
         });

         $('.ui.accordion').accordion({
             exclusive: false,
             selector: {}
         });

     }
 };

 window.App = App;